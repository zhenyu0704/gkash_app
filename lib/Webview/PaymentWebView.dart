import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import '../Payment_Request/PaymentRequest.dart';
import 'dart:convert';

class PaymentWebView extends StatefulWidget {
  final PaymentRequest paymentRequest;
  PaymentWebView({this.paymentRequest});

  @override
  _PaymentWebViewState createState() => new _PaymentWebViewState();
}

class _PaymentWebViewState extends State<PaymentWebView> {
  String url = "";

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: <Widget>[
      Expanded(
        child: Container(
          child: InAppWebView(
            initialUrl: "https://api-staging.pay.asia/api/PaymentForm.aspx",
            onWebViewCreated: (InAppWebViewController controller) {
              var data = ("version=" +
                  Uri.encodeComponent(widget.paymentRequest.version ?? "") +
                  "&CID=" +
                  Uri.encodeComponent(widget.paymentRequest.cid ?? "") +
                  "&v_currency=" +
                  Uri.encodeComponent(widget.paymentRequest.currency ?? "") +
                  "&v_amount=" +
                  Uri.encodeComponent(widget.paymentRequest.amount ?? "") +
                  "&v_cartid=" +
                  Uri.encodeComponent(widget.paymentRequest.cartid ?? "") +
                  "&v_firstname=" +
                  "&signature=" +
                  Uri.encodeComponent(
                      widget.paymentRequest.generateSignature() ?? ""));

              debugPrint("data:" + data);

              controller.postUrl(
                  url: "https://api-staging.pay.asia/api/PaymentForm.aspx",
                  postData: utf8.encode(data));
            },
          ),
        ),
      ),
      Container(
        width: double.infinity,
        child: RaisedButton(
          child: Text("Exit the page"),
          onPressed: () {
            Navigator.pop(context, url);
          },
        ),
      ),
    ]));
  }
}
