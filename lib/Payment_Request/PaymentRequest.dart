import 'package:crypto/crypto.dart';
import 'dart:convert';

class PaymentRequest {
  final String version;
  final String cid;
  final String currency;
  final String amount;
  final String cartid;
  final String signature;

  PaymentRequest(this.version, this.cid, this.currency, this.amount,
      this.cartid, this.signature);

  String generateSignature() {
    String sign = (signature +
            ";" +
            cid +
            ";" +
            cartid +
            ";" +
            amount.replaceAll(".", "") +
            ";" +
            currency)
        .toUpperCase();
    var signaureHash = sha512.convert(utf8.encode(sign));

    return signaureHash.toString().toLowerCase();
  }
}
