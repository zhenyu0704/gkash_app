import 'package:flutter/material.dart';
import 'Payment_Request/PaymentRequest.dart';
import 'Webview/PaymentWebView.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = "Gkash App Integration";
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurpleAccent[700],
          title: Text(title),
        ),
        body: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var amountInput;
    void submitButton(var amount) {
      final paymentRequest = PaymentRequest(
        "1.5.0",
        "M161-U-33",
        "MYR",
        amount,
        DateTime.now().microsecond.toString(),
        "oAhVwtUxfrop4cI",
      );

      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return PaymentWebView(paymentRequest: paymentRequest);
      }));
    }

    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter the amount';
                }
                return null;
              },
              onChanged: (amount) {
                amountInput = amount;
              },
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Amount",
              ),
            ),
            SizedBox(height: 30),
            Container(
              width: double.infinity,
              child: RaisedButton(
                padding: EdgeInsets.all(10),
                textColor: Colors.black,
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    submitButton(amountInput);
                  }
                },
                child: Text('SUBMIT'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
